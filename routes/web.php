<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\HomeController;

Route::get('/', function () {
    return redirect('login'); 
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/newhome', 'HomeController@newindex')->name('newhome');

Route::get('/changePassword','HomeController@showChangePasswordForm')->name('showChangePassp');
Route::get('/recoverypassword', 'Auth\ForgotPasswordController@showFrom')->name('showRecoveryPassword');
Route::post('/changePassword','HomeController@changePassword')->name('changePassword');

/************************** Mantenimiento ********************************/

Route::get('/mantenimiento','MantenimientoController@index')->name('manto');
Route::get('/mantenimiento/img','MantenimientoController@img')->name('img');
Route::get('/mantenimiento/nuevo','MantenimientoController@showNewRequestForm')->name('solicitudManto');
Route::get('/mantenimiento/list','MantenimientoController@showRequests')->name('consultaManto');
Route::get('/mantenimiento/list/atrasado','MantenimientoController@showLateRequests')->name('mntoAtrasados');
Route::get('/mantenimiento/list/terminado','MantenimientoController@showEndRequests')->name('mntoResueltos');
Route::post('/mantenimiento/listTipos','MantenimientoController@getTipos')->name('getTipos');
Route::post('/mantenimiento/listAreas','MantenimientoController@getAreas')->name('getAreas');
Route::post('/mantenimiento/listEquipos','MantenimientoController@getEquipos')->name('getEquipos');
Route::post('/mantenimiento/listProblemas','MantenimientoController@getProblemas')->name('getProblemas');
Route::post('/mantenimiento/guardasolicitud','MantenimientoController@saveRequest')->name('guardasolicitudmanto');
Route::post('/mantenimiento/getSolicitudes','MantenimientoController@getRequest')->name('getSolicitudesManto');
Route::get('/mantenimiento/detalle/{id?}','MantenimientoController@requestDetail')->name('detallesolmant');
Route::post('/mantenimiento/actualiza','MantenimientoController@updateRequest')->name('updatesolicitudm');
Route::get('/mantenimiento/testImage','MantenimientoController@putImage')->name('testImage');
/************************** Recetario ***********************************/
Route::post('/recetas/guardarNuevaReceta', 'RecetarioController@guardarReceta')->name('guardarNR');
Route::get('/recetas','RecetarioController@index')->name('recetas');
Route::get('/recetas/new','RecetarioController@newRecipe')->name('nuevareceta');
Route::get('/recetas/getIngredienteFormat','RecetarioController@getIngredienteFormat')->name('getIngredienteFormat');
Route::get('/recetas/list','RecetarioController@recetas')->name('recetaslista');
Route::post('/recetas/getRecetas','RecetarioController@getRecetas')->name('getRecetas');
Route::get('/recetas/detalle/{id?}','RecetarioController@detalleReceta')->name('detallereceta');
Route::get('/recetas/localizar','RecetarioController@localizar')->name('localizar');
Route::get('/recetas/getIngrediente','RecetarioController@getIngrediente')->name('getIngrediente');
Route::post('/recetas/buscaIng','RecetarioController@findIngrediente')->name('findIngrediente');

/*************************** Biblioteca *********************************/

Route::get('/biblioteca','BibliotecaController@index')->name('biblioteca');

/*************************** Uber Menu **********************************/
Route::get('/ubereats','UberController@index')->name('ubermenu');
Route::get('/ubereats/menu','UberController@menu')->name('menuubereats');
Route::get('/ubereats/getSubseccion','UberController@getSubseccion')->name('subseccion');
/************************** Dashboard ***********************************/

//Route::get('/dashboard','DashboardController@index')->name('dashboard');
Route::get('/finanzas','DashboardController@index')->name('panel');
Route::get('/finanzas/alex','DashboardController@indexAlex')->name('panelAlex');
Route::get('/finanzas/finanzas/{rtype?}','DashboardController@finanzasDashboard')->name('dashfin');
Route::get('/finanzas/operaciones','DashboardController@operacionDashboard')->name('dashop');
Route::get('/finanzas/compras','DashboardController@comprasDashboard')->name('dashcom');
Route::get('/finanzas/rh','DashboardController@rhDashboard')->name('dashrh');
Route::post('/finanzas/kpiFinanzasTable','DashboardController@kpiFinanzas')->name('kpiFinanzasTable');
Route::post('/finanzas/provTable','DashboardController@detProvTable')->name('detProvTable');
Route::post('/finanzas/artTable','DashboardController@detArtTable')->name('detArtTable');
Route::post('/finanzas/detTable','DashboardController@detAlexTable')->name('detAlexTable');
Route::post('/finanzas/rvcDetMajor','DashboardController@rvcDetMajor')->name('rvcDetMajor');
Route::post('/finanzas/rvcDetItem','DashboardController@rvcDetItem')->name('rvcDetItem');
Route::post('/finanzas/kpiOpsTable','DashboardController@kpiOperacionesable')->name('kpiOpsTable');
Route::post('/finanzas/kpiVentasTable','DashboardController@kpiVentasTable')->name('kpiVentasTable');
Route::get('/finanzas/alex/{suc?}','DashboardController@detSucKpi')->name('detSucKpi');
Route::get('/finanzas/redes/','DashboardController@alexRedes')->name('redes');
Route::get('/finanzas/evaluacion/{isPdf?}','DashboardController@evaluacion')->name('evaluacion');
Route::get('/finanzas/comparativo/{isPdf?}/{order?}/{col?}','DashboardController@comparativo')->name('compara');
Route::get('/finanzas/auditi/','DashboardController@alexAuditi')->name('auditi');
Route::get('/finanzas/dcalidad/','DashboardController@alexDCalidad')->name('dcalidad');
Route::get('/finanzas/ubereats/','DashboardController@alexUber')->name('ubercal');
Route::get('/finanzas/rhemps','DashboardController@alexRH')->name('rhemps');
Route::post('/finanzas/saveAudit','DashboardController@guardaAudInterna')->name('guardaaudi');
Route::post('/finanzas/saveCost','DashboardController@guardaCalidad')->name('guardacalidad');
Route::post('/finanzas/saveRHdata','DashboardController@guardaRHdata')->name('guardanemp');
Route::post('/finanzas/saveubereats','DashboardController@guardaUberEats')->name('guardauberc');
Route::post('/finanzas/saveredes','DashboardController@guardaRedes')->name('guardaredes');
Route::post('/finanzas/savesourvey','DashboardController@guardaEncuesta')->name('guardaencuesta');
Route::post('/finanzas/savecap','DashboardController@guardaCapacita')->name('guardacapacita');

Route::get('/finanzas/bitacora/{selsuc?}','DashboardController@bitacora')->name('bitacora');
Route::post('/finanzas/savebitacora','DashboardController@guardaBitacora')->name('guardaBitacora');
Route::post('/finanzas/updatebitacora','DashboardController@actualizaBitacora')->name('actualizaBitacora');
Route::post('/finanzas/deletebitacora','DashboardController@eliminaTaskBitacora')->name('eliminaTaskBitacora');
Route::post('/finanzas/updatetaskbitacora','DashboardController@actualizaTaskBitacora')->name('actualizaTaskBitacora');
Route::get('/finanzas/getbitacora/{idSucursal?}/{tipo?}','DashboardController@getBitacora')->name('getAcciones');
Route::post('/finanzas/getmenbitacora','DashboardController@getMensajesBitacora')->name('getMensajes');
Route::post('/finanzas/guardamensaje','DashboardController@saveMensaje')->name('guardaMensaje');
Route::get('/finanzas/dailySummary/{idSucursal?}','DashboardController@ventaDiariaPDF')->name('resumenDiario');
Route::get('/finanzas/getDailyReport','DashboardController@getDiariaPDF')->name('ventaDiaria');
Route::get('/finanzas/getInventory','DashboardController@getInventory')->name('InvSBO');

/**********************************EK MUSICA***********************************************************/

Route::post('/musica/status','MusicaController@setStatus')->name('statusMusica');

/************************** Vacantes RH ***********************************/

Route::get('/rh/vacantes','RHVacanteController@index')->name('vacantes');
Route::get('/rh/vacantes/nuevavacante','RHVacanteController@showNewRequestForm')->name('nuevavacante');
Route::get('/rh/vacantes/consultavacantes','RHVacanteController@showRequests')->name('consultavacantes');
Route::get('/rh/vacantes/consultacontratados','RHVacanteController@showClosedRequests')->name('consultacontratados');
Route::post('/rh/vacantes/listDeptos','RHVacanteController@getDeptosList')->name('getdeptos');
Route::post('/rh/vacantes/listPuestos','RHVacanteController@getPuestosList')->name('getpuestos');
Route::post('/rh/vacantes/listPuestosCrece','RHVacanteController@getPuestosGrowup')->name('getpuestoscrece');

Route::post('/rh/vacantes/validaPuesto','RHVacanteController@validaPuesto')->name('validapuesto');
Route::post('/rh/vacantes/validanuevoPuesto','RHVacanteController@validanuevoPuesto')->name('validanuevopuesto');
Route::get('/rh/vacantes/empleados','RHVacanteController@showEmployees')->name('empleados');
Route::post('/rh/vacantes/getEmpleados','RHVacanteController@getEmployees')->name('getEmpleados');

Route::post('/rh/vacantes/guardasolicitud','RHVacanteController@saveRequest')->name('guardasolicitud');
Route::post('/rh/vacantes/getSolicitudes','RHVacanteController@getRequest')->name('getSolicitudes');
Route::post('/rh/vacantes/getSolicitudesBaja','RHVacanteController@getDismissRequest')->name('getSolicitudesBaja');
Route::post('/rh/vacantes/getSolicitudesc','RHVacanteController@getClosedRequests')->name('getSolicitudesC');
Route::get('/rh/vacantes/contratados','RHVacanteController@showPendingConfirmation')->name('getContratados');
Route::get('/rh/vacantes/capacitados','RHVacanteController@showPendingFinalConfirmation')->name('getCapacitados');
Route::get('/rh/vacantes/bajas','RHVacanteController@showPendingDismiss')->name('getBajas');
Route::get('/detallevacante/{id?}','RHVacanteController@requestDetail')->name('detallevacante');
Route::get('/rh/empleado/{id?}','RHVacanteController@employeeDetail')->name('detalleempleado');
Route::post('/rh/guardavacante','RHVacanteController@updateVacante')->name('guardavacante');
Route::post('/rh/guardabaja','RHVacanteController@saveBaja')->name('guardabaja');
Route::get('/rh/plantilla','RHVacanteController@showGlobalHeadcount')->name('plantilla');
Route::get('/rh/plantilla/download/{idSucursal}','RHVacanteController@downloadPlantilla')->name('xlsPlantilla');
Route::post('/rh/plantilla/sucursal','RHVacanteController@detPlantillaTable')->name('detPlantillaTable');
Route::post('/rh/vacantes/exportar','RHVacanteController@exportRequest')->name('exportavacantes');
Route::get('/rh/vacantes/consultaRetrasadas','RHVacanteController@showRetrasadas')->name('showRetrasadas');
Route::get('/rh/vacantes/consultaEnTiempo','RHVacanteController@showEnTiempo')->name('showEnTiempo');
Route::get('/rh/vacantes/cronAuth','RHVacanteController@sendAuths')->name('sendAutorizacion');
Route::get('/rh/vacante/aut/{idSolicitud?}/{idAutoriza?}/{accion?}','RHVacanteController@autRequest')->name('autorizasolicitud');
Route::post('/rh/actualizaEmpleado','RHVacanteController@actualizaEmpleado')->name('actualizaEmpleado');


/************************** Pedidos ***********************************/

Route::get('/pedidos','PedidosController@index')->name('pedidos');

Route::get('/nuevopedido','PedidosController@showNewRequestForm')->name('nuevopedido');

Route::get('/consultapedido','PedidosController@showRequests')->name('consultapedido');

Route::get('/consultatraslado','PedidosController@showTransfers')->name('consultatraslado');

Route::get('/detallepedido/{id?}','PedidosController@requestDetail')->name('detallepedido');

Route::get('/detalletraslado/{id?}','PedidosController@transferDetail')->name('detalletraslado');

Route::get('/recibetraslado/{id?}','PedidosController@receiveTransfer')->name('recibirtraslado');

Route::get('/recibepedido/{id?}','PedidosController@receiveOrder')->name('recibirpedido');

Route::get('/conrecibirpedido/{id}','PedidosController@showReceivedOrder')->name('conrecibirpedido');

Route::get('/pedido/{prov}/{id}','PedidosController@printProvRequest')->name('detallepedidoprov');

Route::get('/pedido/getStoreInventory/{ifile}/{inom}','PedidosController@getStoreInventory')->name('getStoreInventory');

Route::get('/pedidoprigo/xls/{id}','PedidosController@printPrigoRequest')->name('xlsFoodPrigo');

Route::get('/pedidos/getArticulo','PedidosController@getItems')->name('getarticulo');

Route::get('/pedidos/getArticuloTrans','PedidosController@getItemsTrans')->name('getarticulotrans');

Route::get('/pedidos/getArticuloRecibir','PedidosController@getItemsReceive')->name('getarticulorecibir');

Route::get('/pedidos/listArticulos','PedidosController@listItems')->name('consultarticulos');

Route::post('/pedidos/gridArticulos','PedidosController@getItemsList')->name('gridArticulos');

Route::post('/pedidos/guardaPedido','PedidosController@saveRequest')->name('guardapedido');

Route::post('/pedidos/guardaPedidoTrans','PedidosController@saveRequestTrans')->name('guardapedidotrans');

Route::post('/pedidos/recibePedido','PedidosController@saveReceiveRequest')->name('recibepedido');

Route::post('/pedidos/getPedidos','PedidosController@getRequest')->name('getPedidos');

Route::post('/pedidos/getTraslados','PedidosController@getTransfer')->name('getTraslados');

Route::post('/pedidos/uploadPedido','PedidosController@uploadRequest')->name('upPedido');

Route::get('/pedidos/sendEmail','PedidosController@sendEmail')->name('sendEmail');

Route::get('/pedidos/sendDesabasto','PedidosController@sendDesabasto')->name('sendDesabasto');

Route::get('/pedidos/traslado/nuevo','PedidosController@showNewTransferForm')->name('nuevoTraslado');

/******************** SAP *****************************/
Route::get('/pedidos/SBO/Items','PedidosController@getSBOItems')->name('ItemsSBO');
Route::get('/nuevopedidokayser','PedidosController@showNewRequestKayserForm')->name('nuevopedidokay');
Route::post('/pedidos/xlsopts','PedidosController@getExcelOpts')->name('getExcelOpts');
Route::get('/pedidoprigo/xls/template/{id}','PedidosController@getRequestTemplate')->name('descargaplantilla');

/************************* QR ***************************/
Route::get('/qr/generator', 'QRGeneratorController@index')->name('qrgen');
Route::post('/qr/generator/download', 'QRGeneratorController@download')->name('qrgendwn');
/************************* Volumio ***************************/
Route::get('/musica','MusicaController@index')->name('musica');