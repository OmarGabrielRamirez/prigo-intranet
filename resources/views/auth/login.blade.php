@extends('layouts.loginApp')

@section('content')
   <!-- Navbar -->
	<nav class="navbar navbar-expand-lg bg-primary navbar-transparent navbar-absolute" color-on-scroll="500">
		<div class="container">
		<div class="navbar-wrapper">
			  <a class="navbar-brand" href="#">Intranet Prigo</a>
		</div>

		</div>
	</nav>
	<!-- End Navbar -->
    <div class="wrapper wrapper-full-page">
            <div class="page-header login-page header-filter" filter-color="black" style="background-image: url('images/Maison-Kayser-pastries.jpg'); background-size: cover; background-position: top center;">
        <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->

            <div class="container">
                <div class="col-md-4 col-sm-6 ml-auto mr-auto">
                    <form method="POST" action="{{ route('login') }}">
						{{ csrf_field() }}
						<div class="card card-login">

							<div class="card-header card-header-warning text-center">
								<img src="images/logo_prigo_mini.png">
							</div>

							<div class="card-body ">
								<p class="card-description text-center">Iniciar sesión</p>

								<span class="bmd-form-group">
									<div class="input-group">
									  <div class="input-group-prepend">
										<span class="input-group-text">
											<i class="material-icons">email</i>
										</span>
									  </div>
										<input type="email"  name="email" autofocus class="form-control" placeholder="Email...">
									@if ($errors->has('email'))
										<span class="help-block">
											<strong>{{ $errors->first('email') }}</strong>
										</span>
									@endif
									</div>
								</span>

								<span class="bmd-form-group">
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text">
												<i class="material-icons">lock_outline</i>
											</span>
										</div>
										<input type="password" class="form-control" name="password" required placeholder="Password...">
										@if ($errors->has('password'))
										<span class="help-block">
										<strong>{{ $errors->first('password') }}</strong>
										</span>
										@endif
									</div>
								</span>
								<a href="{{ route('showRecoveryPassword') }}" id="forgot_pswd">¿Olvidaste tu contraseña?</a>
							</div>
							<div class="card-footer justify-content-center">
								<button class="btn btn-warning btn-link btn-lg" type="submit">Ingresar</button>
							</div>

						</div>
                    </form>

                </div>
            </div>
		</div>
		<footer class="footer ">
				<div class="container">
					<nav class="pull-left">
						<ul>
							<li>
								<a href="https://www.carmelaysal.mx/">
									Carmela & Sal
								</a>
							</li>
							<li>
								<a href="http://maison-kayser.com.mx/">
								   Maison Kayser
								</a>
							</li>
						</ul>
					</nav>
					<div class="copyright pull-right">
						© <script>document.write(new Date().getFullYear())</script>,Grupo Prigo.
					</div>
				</div>
			</footer>
	</div>
@endsection

<style>
a{
    display: block;
    padding-top:10px;
    color:lightseagreen;
	margin-left: 20px;
	font-size: 12px;
}

.wrapper-full-page{
	overflow: hidden;
}
</style>