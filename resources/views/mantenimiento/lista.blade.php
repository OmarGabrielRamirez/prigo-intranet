@extends('layouts.app')
@include('menu.mantenimiento', ['seccion' => 'consultaManto'])
@section('content')
<div class="row">
        <label class="col-sm-1 col-form-label">Problema</label>
        <div class="col-sm-3">
                <div class="form-group bmd-form-group">
                <input id="findPuesto" type="text" class="form-control" >
              </div>
        </div>
        <label class="col-sm-1 col-form-label">Sucursal</label>
        <div class="col-sm-3">
                <div class="form-group bmd-form-group">
                <input id="findSucursal" type="text" class="form-control">
              </div>
        </div>
          <div class="col-sm-1">
            <button id="findVacantebtn" class="btn btn-white btn-round btn-just-icon">
              <i class="material-icons">search</i>
              <div class="ripple-container"></div>
            </button>
          </div>
</div>
<div class="row">
    <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
        <thead>
        <tr>
            <th>Solicitud</th>
            <th>Fecha</th>
            <th>Sucursal</th>		  
            <th>Tipo</th>
            <th>Problema</th>
            <th>Equipo</th>
            <th>Estado</th>
            <th>Acci&oacute;n</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>Solicitud</th>
            <th>Fecha</th>
            <th>Sucursal</th>		  
            <th>Tipo</th>
            <th>Problema</th>
            <th>Equipo</th>
            <th>Estado</th>
            <th>Acci&oacute;n</th>
        </tr>
        </tfoot>
        <tbody>
        </tbody>
    </table>
</div>
@endsection
@section('aditionalScripts')
<script type="text/javascript">

$(document).ready(function() {
    $('#datatables').DataTable({
        "responsive": true,	
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "{{ route('getSolicitudesManto') }}",
            "type": "POST",
			"data": function ( d ) {
                d._token = "{{ csrf_token() }}";
                d.estatus = {{ $estado }}
            }
        },
        "columns": [
            { "data": "idSolicitud" },
            { "data": "fechaCrea" },
            { "data": "sucursal" },
            { "data": "tipo" },
            { "data": "problema" },
            { "data": "equipo" },
			{ "render": function (data, type, row, meta) {
				return "<span style=\"display:block;\"><span>"+row.estado+"</span> <i class=\"pull-right material-icons "+(row.atraso == 1?" text-danger\">highlight_off":" text-success\">check_circle_outline")+"</i>";
				}
			},
			{ "render": function (data, type, row, meta) {
				return "<a href=\"{{ route('detallesolmant') }}/"+row.idSolicitud+"\" class=\"btn btn-link btn-info btn-just-icon like\"><i class=\"material-icons\">open_in_new</i></a>";
				}
			}
        ]
    });

    var table = $('#datatables').DataTable();
 
	$('#findVacantebtn').on( 'click', function () {
		if($("#findSucursal").val() != "")
			table.column(2).search( $("#findSucursal").val() );
		else
			table.column(2).search("");
		if($("#findPuesto").val() != "")
			table.column(3).search( $("#findPuesto").val() );
		else
			table.column(3).search("");
		if($("#findSucursal").val() != "" || $("#findPuesto").val() != "")
			table.draw();
	} );
	$("#datatables_filter").hide();
});

</script>

@endsection