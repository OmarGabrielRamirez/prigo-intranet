@extends('layouts.newlay')
@include('menu.uber', ['seccion' => 'menu'])
@section('content')
<div class="row" style="z-index: 1050!important;">
    <div class="col-md-12">
        <div class="card" style="z-index: 1050!important;">
            <div class="card-header">
                <h4 class="card-title">Consultar</h4>
            </div>
            <div class="card-content" style="padding: 10px;">
                <div class="row">
                    <div class="col-md-12">
                        <select onchange="subsec(this.value)" class="selectpicker">
                            <option value="" @if(empty($selSuc)) selected @endif> Seleccione una seccion</option>
                            @if(!empty($secciones))
                                @foreach($secciones as $seccion)
                                    <option @if(!empty($selSec) && $selSec==$seccion->id) selected @endif value="{{ $seccion->id }}">{{ $seccion->seccion}}</option>
                                @endforeach
                            @endif
                        </select>
                        <select id="subSeccion" onchange="menu(this.value)" class="selectpicker">
                                <option value=""  selected> Seleccione una subseccion</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
      </div>
      <div class="col-md-12">
            <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Menu</h4>
                    </div>
                    <div class="card-content" style="padding: 10px;">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                          <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                    <h4 class="panel-title">
                                    Collapsible Group Item #1
                                    <i class="material-icons">keyboard_arrow_down</i>
                                    </h4>
                                </a>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                              <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                              </div>
                            </div>
                          </div>
                          <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <h4 class="panel-title">
                                  Collapsible Group Item #2
                                  <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                              </a>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false">
                              <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                              </div>
                            </div>
                          </div>
                          <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <h4 class="panel-title">
                                  Collapsible Group Item #3
                                  <i class="material-icons">keyboard_arrow_down</i>
                                </h4>
                              </a>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
                              <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
      </div>
</div>
@endsection
@section('jsimports')
<script src="{{ asset('MaterialBS/js/plugins/bootstrap-selectpicker.js') }}"></script>
  <script src="{{ asset('MaterialBS/js/plugins/jquery.select-bootstrap.js') }}"></script>
  <script src="{{ asset('MaterialBS/js/plugins/bootstrap-tagsinput.js') }}"></script>
  <script src="{{ asset('MaterialBS/assets-for-demo/js/modernizr.js') }}"></script>
@endsection
@section('aditionalScripts')
  
  <script type="text/javascript">
    function subsec(seccion)
    {
        if(seccion)
        {
            swal({
                title: 'Cargando...',
                allowEscapeKey: false,
                allowOutsideClick: false,
                showCancelButton: false,
                showConfirmButton: false,
                text: 'Espere un momento...'
            });

            $.ajax({
                type: "GET",
                url: "{{ route('subseccion') }}",
                data: { "seccion": seccion },
                success: function(msg){
                    var len = msg.data.length;
                    var l = 0;
                    $("#subSeccion").empty();
                    $("#subSeccion").append("<option selected value='0'>Seleccione una subseccion</option>");
                    if(len > 0)
                    {   
                        while(l<len)
                        {
                            $("#subSeccion").append("<option value='"+msg.data[l].id+"'>"+msg.data[l].subseccion+"</option>");
                            l++;
                        }
                        
                    }
                    $("#subSeccion").selectpicker('refresh');
                    swal.close();

                },
                error: function(){
                    swal({
                        type: 'error',
                        title: 'Oops...',
                        text: 'Algo ha salido mal!',
                        footer: 'Problemas? sit@prigo.com.mx	',
                    });
                }
            });	
        }
    }
    function menu(subseccion)
    {
        
    }
  </script>
@endsection