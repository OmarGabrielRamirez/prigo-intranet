@extends('layouts.app')
@include('menu.pedidos')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card card-body printableArea">
			<h3><b>Traslado <span>#{{ $pedido[0]->idPedido }}</span></b> - {{ $pedido[0]->usuario }}</h3>
			<h6>Referencia SAP: {{ $pedido[0]->codigoSAP }}<h6>
			<hr>
			<div class="row">
				<div class="col-md-12">
					<div class="pull-left">
						<address>
							<h3><b class="text-danger">{{ $pedido[0]->sucursal }}</b></h3>
							<h5> {{ $pedido[0]->fechaRequerida }} </h5>
							<p class="text-muted m-l-5"></p>
						</address>
					</div>
					<div class="pull-right text-right">
						<address>
							<p class="m-t-30"><b>{{ $pedido[0]->fecha }}</b><br>{{ $pedido[0]->hora }}</p>
						</address>
					</div>
				</div>
				<div class="col-md-12">
					<div class="table-responsive m-t-40" style="clear: both;">
						<table class="table table-hover">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="text-center">Codigo SAP</th>
									<th>Articulo</th>
									<th class="text-center">Categoria</th>
									<th>U de M</th>
									<th class="text-right">Cantidad</th>
								</tr>
							</thead>
							<tbody>
							@foreach($partidas as $partida)
								<tr>
									<td class="text-center">{{ $loop->index + 1 }}</td>
									<td class="text-center">{{ $partida->CodPrigo }}</td>									
									<td class="text-left">{{ $partida->descripcion }}</td>	
									<td class="text-center">{{ $partida->cat }}</td>
									<td class="text-left">{{ $partida->UnidadPrg }}</td>	
									<td class="text-right">{{ $partida->cantidad }}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				<div class="col-md-12">
					<div class="pull-right m-t-30 text-right">
						<p>{{ $pedido[0]->comentario }}</p>
					</div>
					<div class="clearfix"></div>
					<hr>
					<!--div class="text-right">
						<button id="print" class="btn btn-default btn-outline" type="button"> <span><i class="fa fa-print"></i> Print</span> </button>
					</div-->
				</div>
			</div>
		</div>
	</div>
</div>
@endsection