@extends('layouts.pro')
@include('menu.recetario', ['seccion' => 'receta'])
@section('content')

<div class="row">
        <div class="col-md-12">
            <div class="card ">
                <div class="card-header card-header-orange card-header-text">
                    <div class="card-text">
                        <h4 class="card-title">Nueva receta</h4>
                    </div>
                </div>
                <div class="card-body ">
                    <form method="get" action="/" class="form-horizontal" id="formReceta">
                        <input type="hidden" value="0" id="ingId">
                        <input type="hidden" value="0" id="ingCosto">                        
                        <input type="hidden" value="" id="ingCod">
                        <input type="hidden" value="" id="ingTipo">
                        <input type="hidden" value="" id="ingName">
                        <input type="hidden" value="" id="ingUnit">
                        <div class="row">
                            <label class="col-sm-2 col-form-label">Platillo</label>
                            <div class="col-sm-6">
                                <div class="form-group bmd-form-group">
                                    <input id="nombrePlatilloInput" type="text" class="form-control" name="nombre_receta" style="text-transform:uppercase;"  onkeyup="mayus(this);">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">Tipo de receta</label>
                            <div class="col-sm-6">
                                <div class="form-group bmd-form-group">
                                    <select id="tipoReceta" name="tipo_receta" class="tipoReceta" style="width: 100%">
                                        <option  selected="selected">Seleccione un tipo de receta</option>
                                        <option>PRIGO (PRODUCCIÓN)</option>
                                        <option>KAYSER (TIENDA)</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">Unidad</label>
                            <div class="col-sm-6">
                                <div class="form-group bmd-form-group">
                                    <select id="slctUnidad" class="slctUnidadReceta" name="unidad_receta" style="width: 100%" disabled >
                                        <option  selected="selected">Seleccione una opción</option>
                                        <option>ORD</option>
                                        <option>PZA</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">Cantidad</label>
                            <div class="col-sm-6">
                                <div class="form-group bmd-form-group">
                                    <input id="cantidadReceta" type="number" class="form-control" name="cantidad_receta" min="0" style="text-transform:uppercase;"disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">Subreceta</label>
                            <div class="col-sm-6">
                                <div class="form-group bmd-form-group">
                                    <select id="slctSub" name="subreceta" class="slctSubcreceta" style="width: 100%" disabled>
                                        <option  selected="selected">Seleccione una opción</option>
                                        <option>SÍ</option>
                                        <option>NO</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group bmd-form-group">
                                    <select id="slctArticulo" class="getItemSelect" style="width: 100%" disabled><option>Seleccione un Ingrediente</option></select>
                                </div>
                            </div>
                            <div class="col-sm-4" style="display: inline-block;vertical-align: middle;float: none;">
                                <button type="button" id="addArtBtn" class="btn btn-success btn-round btn-just-icon" disabled><i class="material-icons">control_point</i></button> 
                                <button type="button" id="addIngreNoExistente" data-toggle="modal" data-target="#modalLoginForm" class="btn btn-info btn-round"  disabled><i class="material-icons" >playlist_add</i> Ingrediente no existente</button>                 
                            </div>
      
                        </div>
                        <div class="row">
                            <div class="col-sm-12 table-wrapper-2">
                                <table id="tblArts" class="table table-striped">
                                    <thead>
                                        <tr>
                                        <th class='col-xs-1'>Codigo</th>
                                        <th class='col-xs-6'>Ingrediente</th>
                                        <th class='col-xs-1'>Cant Sucio</th>
                                        <th class='col-xs-1'>Cant Limpia</th>
                                        <th class='col-xs-1'>UM</th>
                                        <th class='col-xs-1'>Costo</th>
                                        <th class='col-xs-1'></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-sm-12">
                                Costo de la receta:  $ <span id="lblCostoReceta" name="costo_receta"></span>
                                <input id="lblCostoReceta_hidden" type="hidden" class="form-control" name="precio_receta">
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 col-form-label">Comentarios</label>
                            <div class="col-sm-10">
                                <div class="form-group bmd-form-group">
                                    <textarea id="txtArea" name="comentario" class="form-control" disabled></textarea>
                                    <span class="bmd-help">Detalles o excepciones del pedido</span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group bmd-form-group">
                                    <button id="recetaSave" type="button" class="btn btn-info btn-round" disabled>Guardar Receta</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
</div>
<!-- Modal agregar ingrediente no existente  -->
<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Agregar Ingrediente no existente</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-4">
        <div class="col-sm-9">
          <input type="text" id="nombreIngrediente" class="form-control"  style="text-transform:uppercase;" style="width: 400px !important;" onkeyup="mayus(this);">
          <label data-error="wrong" data-success="right" for="nombreIngrediente">Nombre Ingrediente</label>
        </div>
        <div class="col-sm-9">
            <select id="slcUM" class="form-control" style="width: 40%">
                <option>Kilos</option>
                <option>Litros</option>
            </select>
            <label data-error="wrong" data-success="right" for="cantidadSucia">UM</label>
        </div>
        <div class="col-sm-9">
          <input type="text" id="cantidadSucia" class="form-control" style="width: 110px !important;">
          <label data-error="wrong" data-success="right" for="cantidadSucia">Cantidad Sucia</label>
        </div>
        <div class="col-sm-9">
            <input type="text" id="cantidadLimpia" class="form-control" style="width: 110px !important;">
            <label data-error="wrong" data-success="right" for="cantidadLimpia">Cantidad Limpia</label>
        </div>
        <div class="col-sm-9">
            <input type="text" id="costo" class="form-control" style="width: 110px !important;" placeholder="40.00">
            <label data-error="wrong" data-success="center" for="costo">Costo</label>
        </div>
      </div>
      <div class="modal-footer d-flex justify-content-center">
        <button id="ingredienteSave" type="button" class="btn btn-info btn-round" >Guardar</button>
      </div>
    </div>
  </div>
</div>
  <!-- Modals -->
@endsection
@section('aditionalScripts')
<style>
    .table-wrapper-2 {
        display: block;
        max-height: 300px;
        overflow-y: auto;
        -ms-overflow-style: -ms-autohiding-scrollbar;
    }
    /* -------------------------------------- */
    /* Estilos para el select de unidades base */
    /* -------------------------------------- */
    .custom-select {
        position: relative;
        font-family: Arial;
    }

    .custom-select select {
        display: none; /*hide original SELECT element: */
    }

    .select-selected {
        background-color: DodgerBlue;
    }

    /* Style the arrow inside the select element: */
    .select-selected:after {
        position: absolute;
        content: "";
        top: 14px;
        right: 10px;
        width: 0;
        height: 0;
        border: 1px solid transparent;
        border-color: #fff transparent transparent transparent;
    }

    /* Point the arrow upwards when the select box is open (active): */
    .select-selected.select-arrow-active:after {
        border-color: transparent transparent #fff transparent;
        top: 7px;
    }

    /* style the items (options), including the selected item: */
    .select-items div,.select-selected {
        color: #ffffff;
        padding: 8px 16px;
        border: 1px solid transparent;
        border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;
        cursor: pointer;
    }

    /* Style items (options): */
    .select-items {
        position: absolute;
        background-color: DodgerBlue;
        top: 100%;
        left: 0;
        right: 0;
        z-index: 99;
    }

    /* Hide the items when the select box is closed: */
    .select-hide {
        display: none;
    }

    .select-items div:hover, .same-as-selected {
        background-color: rgba(0, 0, 0, 0.1);
    }
/* -------------------------------------- */
/* -------------------------------------- */
</style>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        
//-------------------------------------------------------------------------------------------------//
//---------- Varibles donde se almacenara el resultado de los select y del input-------------------//
    var valorUnidadReceta = "";
    var valorSubreceta = "";
    var tipoRecetaSlct = "";
    var nombrePlatillo = "";
    var costoTotal = 0.0;
    var arrayT = new Array();
//--------------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------//
//----------- Obteniendo los del select de tipo de receta-------------------------------------------//
    $(document).on('change', '#tipoReceta', function(event) {
        tipoRecetaSlct = $(this).val();
        
        arrayT.push(tipoRecetaSlct);
        
        for(var i =1; i < arrayT.length; i++){
            if(arrayT[i] =! arrayT[i+1]){
                swal({
				    title: "Estas segur@?",
				    text: "Se reiniciara el formulario al hacer el cambio de tipo de receta",
				    type: "warning",
				    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
					allowOutsideClick: false,
					confirmButtonText: 'Entendido',
					cancelButtonText: 'Cancelar'
			}).then((result) =>{
                if (result.value) {
                    setTimeout(function() { window.location.href = "/recetas/new";}, 900);
                }else{

                }
            });
            }
        }
//--------------------------------------------------------------------------------------------------//  
//-----Comprobando que se haya seleccionado alguna opción para habilitar campos---------------------//
//--------------------------------------------------------------------------------------------------//
        if(tipoRecetaSlct != "Seleccione un tipo de receta"){
            $('#slctSub').prop('disabled', false);
            $('#slctUnidad').prop('disabled', false);
            $('#slctArticulo').prop('disabled', false);
            $('#txtArea').prop('disabled', false);
            $('#recetaSave').prop('disabled', false);
            $('#addArtBtn').prop('disabled', false);
            $('#addIngreNoExistente').prop('disabled', false);
            $('#cantidadReceta').prop('disabled', false);
            

        }else{
//--------------------------------------------------------------------------------------------------//
//-------Deshabilitando campos si no hay ninguna selección de receta y alerta de error--------------//
//--------------------------------------------------------------------------------------------------//
            $('#slctSub').prop('disabled', true);
            $('#slctUnidad').prop('disabled', true);
            $('#slctArticulo').prop('disabled', true);
            $('#txtArea').prop('disabled', true);
            $('#recetaSave').prop('disabled', true);
            $('#addArtBtn').prop('disabled', true);
            $('#addIngreNoExistente').prop('disabled', true);
            $('#cantidadReceta').prop('disabled', true);
            swal({
			  type: 'error',
			  title: 'Oops...',
			  text: 'Seleccione un tipo de receta!',
			  footer: 'Problemas? sit@prigo.com.mx',
			});
        }
        

    });

    $(document).on('change', '#slctSub', function(event) {
        valorSubreceta = $(this).val();
    });      

    $(document).on('change', '#slctUnidad', function (event){
        valorUnidadReceta = $(this).val();
  
    })
  //---------------------------------------------------------------------------------------//
  //-------------------Evento para guardar datos de la receta nueva -----------------------//
    $("#recetaSave").click(function(e){   
        nombrePlatillo = $("#nombrePlatilloInput").val();
        if($('.item-added').length > 0 ){
			swal({
				    title: "Estas segur@?",
				    text: "La receta será enviada para su autorización",
				    type: "warning",
				    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
					allowOutsideClick: false,
					confirmButtonText: 'Enviar receta',
					cancelButtonText: 'Cancelar'
			}).then((result) =>{
                if (result.value) {
				$('.button').prop('disabled', true);
                swal({
                  title: 'Guardando...',
				  allowEscapeKey: false,
				  allowOutsideClick: false,
				  showCancelButton: false,
				  showConfirmButton: false,
				  text: 'Espere un momento...'     
                });
                $.ajax({
                    type: "POST",
                    url: "{{route('guardarNR')}}",
                    data: $('form.form-horizontal').serialize(),
                    success: function(msg){
                        swal({
							type: 'success',
                            title: 'Tu receta fue enviada!',
                            icon: 'success',
                            showConfirmButton: false,
                            timer: 9000,
                        });	
                        setTimeout(function() {
                            window.location.href = "/recetas/new";
                        }, 2000);
                        },
                    error: function(){
                        swal({
						  type: 'error',
						  title: 'Oops...',
						  text: 'Algo ha salido mal!',
						  footer: 'Problemas? sit@prigo.com.mx	',
						});
                    }
                    
                });
            }else{
                swal({
						  type: 'error',
						  title: 'Cancelaste el envio de la receta',
				});
            }
            });
            }else if (valorUnidadReceta == "" && "Seleccione una opción"){
                swal({
			        type: 'error',
			        title: 'Oops...',
			        text: 'Seleccione una unidad para la receta!',
			        footer: 'Problemas? sit@prigo.com.mx',
			});
            }else if (valorSubreceta == "" && "Seleccione una opción"){
                swal({
			        type: 'error',
			        title: 'Oops...',
			        text: 'Seleccione si será una subreceta o no!',
			        footer: 'Problemas? sit@prigo.com.mx',
			});
            }else if(nombrePlatillo == ""){
                swal({
			        type: 'error',
			        title: 'Oops...',
			        text: 'Ingrese un nombre para la receta!',
			        footer: 'Problemas? sit@prigo.com.mx',
			    });
            }else if($('.item-added').length  == 0 ){
                swal({
			        type: 'error',
			        title: 'Oops...',
			        text: 'Ingrese al menos un ingrediente!',
			        footer: 'Problemas? sit@prigo.com.mx',
			    });

            }
                

//------------------Obteniendo valor del nombre de la nueva receta------------------------//
    nombrePlatillo = $("#nombrePlatilloInput").val();
//---------------------------------------------------------------------------------------//

    });
 //---------------------------------------------------------------------------------------//

    $('.tipoReceta').select2({
		theme: "bootstrap"
    });

    $('.slctSubcreceta' ).select2({
		theme: "bootstrap"
    });

    $('.slctUnidadReceta').select2({
		theme: "bootstrap"
    });

    $('.getItemSelect').select2({
		theme: "bootstrap",
		ajax: {
		url: "{{ route('getIngredienteFormat') }}",
		dataType: 'json',
		delay: 250,
		data: function (params) {
		  return {
			q: params.term, 
			page: params.page
		  };
		},
		processResults: function (data, params) {
			params.page = params.page || 1;
			return {
				results: data.items,
				pagination: {
				  more: (params.page * 30) < data.total_count
				}
			};
		},
		cache: true
		},
		minimumInputLength: 3,
		placeholder: 'Search for a repository',
		escapeMarkup: function (markup) { return markup; },
		templateResult: formatRepo,
		templateSelection: formatRepoSelection
	});
	
	function formatRepo (repo) {
         if (repo.loading) {
                return repo.text;
        }

        var markup = "<div class='select2-result-repository clearfix'>" +
        "<div class='select2-result-repository__meta'>" +
        "<div class='select2-result-repository__title'>" + repo.nombre + "</div>";
        markup += "<div class='select2-result-repository__description'>" + (repo.tipo ==1?"Ingrediente":"Subreceta") + "</div>";
        markup += "</div></div>";
        return markup;
    }

    function formatRepoSelection (repo) {
	        $("#ingId").val(repo.id);	
	        $("#ingCod").val(repo.cod);	
	        $("#ingTipo").val(repo.tipo);
            $("#ingName").val(repo.nombre);
            $("#ingCosto").val(repo.precio);
	        $("#ingUnit").val(repo.unidad);
	return repo.nombre || repo.id;
    }

    function recalc(el){
        var id = el.id.toString();
        id = id.substr(0,id.length-4);
        $("#"+id).empty();
        $("#lbl"+id).text(el.value*$("#"+id).val());
        console.log(el.value*$("#"+id).val());
    }   

    function isNumberKey(evt, el){
	    var charCode = (evt.which) ? evt.which : evt.keyCode;
	    if (charCode != 46 && charCode > 31 
	    && (charCode < 48 || charCode > 57))
        return false;
	    return true;
    }

    $('.tipoUnidades').select2({
		theme: "bootstrap"
    });

//------Evento para agregar ingrediente a tabla------//
    $('#addArtBtn').click(function(){ 

//------Obteniendo el id del ingrediente y parseandolo a String para hacer la comparacion -----//
    var idValue = ($("#ingId").val());
    var idString = String(idValue);
    var idIngredienteNoSeleccionado = "Seleccione un Ingrediente";

    if(idString == idIngredienteNoSeleccionado){
		swal({
		    type: 'error',
	        title: 'Oops...',
		    text: 'Seleccione un articulo de la lista!',
		    footer: 'Problemas? sit@prigo.com.mx',
		});			
    }else{
        var id = $("#ingId").val();
        var cod = $("#ingCod").val();
        var name = $("#ingName").val();
        var unit = $("#ingUnit").val();
        var costo = parseFloat($("#ingCosto").val()); //-----Parseamos el string a float para trabajar directament----//
        var conv= $("#ingTipo").val();

        var nombreIngre = null;
        nombreIngre = $("#tblArts #nombreIngre").text();

        if( nombreIngre.indexOf(name) > -1){
            swal({
			  type: 'error',
			  title: 'Oops...',
			  text: 'Ya seleccionaste este ingrediente!',
			  footer: 'Problemas? sit@prigo.com.mx',
			});
            
        }else{
            $('#tblArts tbody').append("<tr id='trArt"+id+"' class='item-row form-group'><td class='col-xs-1'><input type='hidden' name='costo[]' id='CostoIng"+id+"' value='"+costo+"'><input type='hidden' id='idArt' class='item-added' value='"+id+"' name='id[]'>"+id+"</td><td class='col-xs-6'><input type='hidden' name='nombre_ingrediente[]' id='NombreIng"+id+"' value='"+name+"'>"+name+"</td><td class='col-xs-1'><input type='text' value='1' class='item-added-qty form-control' id='CantidadSucia"+id+"Cant' style='width: 70px !important;' name='cantidadSucia[]' onkeyup='recalc(this)' onkeypress='return isNumberKey(event, this)'></td><td class='col-xs-1'><input type='text' value='1' class='item-added-qty form-control' id='CostoIngLimpia"+id+"Cant' style='width: 70px !important;' name='cantidadLimpia[]' onkeyup='recalc(this)' onkeypress='return isNumberKey(event, this)'></td><td class='col-xs-1'><select name='unidad_ingre[]'class='custom-select'><option value='Kilos'>Kilos</option><option value='Litros'>Litros</option></select></td><td class='col-xs-1' id='lblCostoIng'>"+costo+"</td><td class='td-actions col-xs-1'><button type='button' rel='tooltip' data-placement='left' title='' class='btn btn-link remove-btn' data-original-title='Remove item'><i class='material-icons'>close</i></button></td></tr>");
            //----- Realizamos la suma de todos los costos -----//
            costo = isNaN(costo) ? 0: costo;
            costoTotal += costo;
            ///----- Seteamos el costo total de la receta ------ //
            $("#lblCostoReceta").text(costoTotal.toFixed(2));
            $("#lblCostoReceta_hidden").val(costoTotal.toFixed(2));
            $('#theModal').modal('hide');
          
        }
    }

//-------Acción para que funcione el boton "x" para eliminar ingredientes de la tabla y actualiza la cuenta cuando se elimina el articulo correspondientes----//
    $(".remove-btn").click(function(){
		    $(this).parents(".item-row").remove();
            costoTotal -=costo;

            if(costoTotal < 0){
                costoTotal = 0;
            }
            $("#lblCostoReceta").text(costoTotal.toFixed(2));
            $("#lblCostoReceta_hidden").val(costoTotal.toFixed(2));         
	});

    // if($("#ingId").val() != 0)
    
// 		{
// 			var id= $("#ingId").val();
// 			var cod= $("#ingCod").val();
// 			var name= $("#ingName").val();
//             var unit= $("#ingUnit").val();
//             var costo= parseFloat($("#ingCosto").val()); //-----Parseamos el string a float para trabajar directament----//
// 			var conv= $("#ingTipo").val();
			
// 			$('#tblArts tbody').append("<tr id='trArt"+id+"' class='item-row form-group'><td class='col-xs-1'><input type='hidden' name='costo[]' id='CostoIng"+id+"' value='"+costo+"'><input type='hidden' class='item-added' value='"+id+"' name='id[]'>"+id+"</td><td class='col-xs-6'>"+name+"</td><td class='col-xs-1'><input type='text' value='1' class='item-added-qty form-control' id='CostoIng"+id+"Cant' style='width: 70px !important;' name='cantidad[]' onkeyup='recalc(this)' onkeypress='return isNumberKey(event, this)'></td><td class='col-xs-1'><input type='text' value='1' class='item-added-qty form-control' id='CostoIngLimpio"+id+"Cant' style='width: 70px !important;' name='cantidad[]' onkeyup='recalc(this)' onkeypress='return isNumberKey(event, this)'></td><td class='col-xs-1'><select class='custom-select'><option value='Kilos'>Kilos</option><option value='Litros'>Litros</option></select></td><td class='col-xs-1' id='lblCostoIng'>"+costo+"</td><td class='td-actions col-xs-1'><button type='button' rel='tooltip' data-placement='left' title='' class='btn btn-link remove-btn' data-original-title='Remove item'><i class='material-icons'>close</i></button></td></tr>");
// //----- Realizamos la suma de todos los costos -----//
//             costo = isNaN(costo) ? 0: costo;
//             costoTotal += costo;

            // $("#ingId").val(0);
            // $("#ingCosto").val(0);
			// $("#ingCod").val("");
			// $("#ingName").val("");
			// $("#ingTipo").val("");
            // $("#ingUnit").val("");

            
            // var val =  0
            // var costo = 0;

            // $("input[name='costo[]']").each(function(index){
            //     console.log($( this ).attr("id"));
            //     val = $( this ).val()?$( this ).val():0;
            //     costo += val*$("#"+$( this ).attr("id")+"Cant").val();
            //     console.log(val*$("#"+$( this ).attr("id")+"Cant").val());
     
            // });
            
            // $("#lblCostoIng").empty();
            // $("#lblCostoIng").text(costoTotal);
			
			// $(".remove-btn").click(function(){
			// 	$(this).parents(".item-row").remove();
			// });
			
			$( "[name='cantidad[]']" ).keyup(function() {
				var val = $( this ).val()?$( this ).val():0;
                var conv = $("#"+$( this ).attr("id")+"Conv").val()?$("#"+$( this ).attr("id")+"Conv").val():1;
                $("#"+$( this ).attr("id")+"Prg").empty();
                $("#"+$( this ).attr("id")+"Prg").text(val*conv);
			});
			
			    $("#CantArt"+id).focus();
			    $("#CantArt"+id).select();
		 
		// else
		// {
		// 	swal({
		// 	  type: 'error',
		// 	  title: 'Oops...',
		// 	  text: 'Seleccione un articulo de la lista!',
		// 	  footer: 'Problemas? sit@prigo.com.mx',
		// 	});
		// }
} );

//--------------------Evento para guardar nuevo ingrediente ------------------------------//
//----------------------------------------------------------------------------------------//
$('#ingredienteSave').click(function(){ 
    
//------------Obteniendo fecha actual que servira para el codigo del ingrediente nuevo-------------------//
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth()+1;
    var yyyy = hoy.getFullYear();
    
//-------------------Agregando ceros a mes y dia a la fecha obtenida----------------------//
    function addZero (i){
        if(i<10){
            i = '0' + 1;
        }
        return i;
    }
    
    dd=addZero(dd);
    mm=addZero(mm);

    var id =  "PENDI-" + dd + mm;
    var nombreIngre = $("#nombreIngrediente").val();
    var unidadMedida = $("#slcUM").val();
    var cantidadSucia = $("#cantidadSucia").val();
    var cantidadLimpia = $("#cantidadLimpia").val();
    var costo = parseFloat( $("#costo").val());

    if( nombreIngre && unidadMedida && cantidadSucia && cantidadLimpia && costo != ''){
        $('#tblArts tbody').append("<tr id='trArt"+id+"'class='item-row form-group'><td class='col-xs-1'><input type='hidden' name='costo[]' id='CostoIng"+id+"' value='"+costo+"'><input type='hidden' class='item-added' value='"+id+"' name='id[]'>"+id+"</td><td class='col-xs-6'><input type='hidden' name='nombre_ingrediente[]' id='NombreIng"+id+"' value='"+nombreIngre+"'>"+nombreIngre+"</td><td class='col-xs-1'><input type='text' value='"+cantidadLimpia+"' class='item-added-qty form-control' id='CostoIng"+id+"Cant' style='width: 70px !important;' name='cantidadLimpia[]' onkeyup='recalc(this)' onkeypress='return isNumberKey(event, this)'></td><td class='col-xs-1'><input type='text' value='"+cantidadSucia+"' class='item-added-qty form-control' id='CostoIngLimpio"+id+"Cant' style='width: 70px !important;' name='cantidadSucia[]' onkeyup='recalc(this)' onkeypress='return isNumberKey(event, this)'></td><td class='col-xs-1'><select class='custom-select' name='unidad_ingre[]' ><option value='Kilos'>Kilos</option><option value='Litros'>Litros</option></select></td><td class='col-xs-1' id='lblCostoIng"+id+"'>"+costo+"</td><td class='td-actions col-xs-1'><button type='button' rel='tooltip' data-placement='left' title='' class='btn btn-link remove-btn' data-original-title='Remove item'><i class='material-icons'>close</i></button></td></tr>");
            costo = isNaN(costo) ? 0: costo;
            costoTotal += costo;
            $("#lblCostoReceta").text(costoTotal.toFixed(2));
            $(".remove-btn").click(function(){ $(this).parents(".item-row").remove();});
            
            $("#nombreIngrediente").val("");
            $("#cantidadSucia").val("");
            $("#cantidadLimpia").val("");
            $("#costo").val("");
            $('#modalLoginForm').modal('hide');

            


    }else{
        swal({
			  type: 'error',
			  title: 'Oops...',
			  text: 'Ingrese todos los datos',
			  footer: 'Problemas? sit@prigo.com.mx',
			});
    }



});

//------------------Funcion para convertir a mayusculas todo lo que ingresen en el nombre del ingrediente--------------------//
function mayus(e) {
    e.value = e.value.toUpperCase();
}



</script>
@endsection