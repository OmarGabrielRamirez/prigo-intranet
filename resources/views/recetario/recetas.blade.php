@extends('layouts.app')
@include('menu.recetario', ['seccion' => 'receta'])
@section('content')
<div class="row">
    <label class="col-sm-1 col-form-label">Receta</label>
    <div class="col-sm-3">
        <div class="form-group bmd-form-group">
            <input id="findReceta" type="text" class="form-control" >
        </div>
    </div>
    <div class="col-sm-1">
        <button id="findRecetabtn" class="btn btn-white btn-round btn-just-icon">
        <i class="material-icons">search</i>
        <div class="ripple-container"></div>
        </button>
    </div>
    <div class="col-sm-1">
        <button id="addRecetabtn" class="btn btn-success btn-round btn-just-icon">
        <i class="material-icons">control_point</i>
        <div class="ripple-container"></div>
        </button>
    </div>
</div>
<div class="row">
<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
	<thead>
	  <tr>
		  <th>Receta</th>
		  <th>Unidad</th>		  
          <th>Cantidad</th>
          <th>Costo</th>
		  <th>Actualizacion</th>
		  <th>Estado</th>
		  <th>Acci&oacute;n</th>
	  </tr>
	</thead>
	<tfoot>
	  <tr>
            <th>Receta</th>
            <th>Unidad</th>		  
            <th>Cantidad</th>
            <th>Costo</th>
            <th>Actualizacion</th>
            <th>Estado</th>
            <th>Acci&oacute;n</th>
	  </tr>
	</tfoot>
	<tbody>
	</tbody>
</table>
</div>
@endsection
@section('aditionalScripts')
  <script type="text/javascript">

$(document).ready(function() {
    $('#datatables').DataTable({
        "responsive": true,	
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "{{ route('getRecetas') }}",
            "type": "POST",
			"data": function ( d ) {
                d._token = "{{ csrf_token() }}";
            }
        },
        "columns": [
            { "data": "receta" },                      
            { "data": "unidad" },
            { "data": "cantidad" },
            { "data": "costo" },
            { "data": "fechaModifica" },
            { "data": "estado" },
			{ "render": function (data, type, row, meta) {
				return "<a href=\"{{ route('detallereceta') }}/"+row.idReceta+"\" class=\"btn btn-link btn-info btn-just-icon like\"><i class=\"material-icons\">open_in_new</i></a>";
				}
			}
        ]
    });

    var table = $('#datatables').DataTable();
    $("#addRecetabtn").on('click', function(){
        window.location.replace("{{ route('nuevareceta') }}");
    });
	$('#findRecetabtn').on( 'click', function () {
		if($("#findReceta").val() != "")
			table.column(0).search( $("#findReceta").val() );
		else
			table.column(0).search("");

		if($("#findReceta").val() != "")
			table.draw();
	} );
	$("#datatables_filter").hide();
});

</script>

@endsection