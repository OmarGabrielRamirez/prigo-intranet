@extends('layouts.app')
@include('menu.recetario', ['seccion' => 'receta'])
@section('content')
<div class="row">
        <div class="col-md-12 ml-auto">
            {{ $receta->receta }}
        </div>
</div>
<div class="row">
    <div class="col-md-12 ml-auto">
        <table class="table table-condensed table-striped">
            <thead>
                <tr>
                    <th>Ingrediente</th>							
                    <th>Unidad</th>
                    <th>Cantidad</th>
                    <th>Costo</th>
                </tr>
            </head>
            <tbody> 
                    @php
                        $totalPrice =0;
                    @endphp
                @foreach($partidas AS $dato)
                    <tr><td class="text-left">{{ $dato->nombre }}</td><td  class="text-center">{{ $dato->unidadIng }}</td><td  class="text-right">{{ $dato->cantidad }}</td><td  class="text-right">{{ number_format($dato->cantidad*$dato->avgPrice,2,".",",")  }}</td></tr>
                    @php
                        $totalPrice +=  $dato->cantidad*$dato->avgPrice;
                    @endphp
                @endforeach
                    <tr><td></td><td></td><td>Costo Total:</td><td class="text-right">{{ number_format($totalPrice,2,".",",")  }}</td></tr>
            </tbody>
        </table>
    </div>
</div>
@endsection