@section('appmenu')
<li class="nav-item active">
	<a class="nav-link" href="{{ route('recetas') }}" aria-expanded="true">
		<i class="material-icons">local_dining</i>
		<p> Recetario <b class="caret"></b> </p>
    </a>
	<div class="collapse show" id="pagesExamples" style="">
		<ul class="nav">
			<li class="nav-item @if($seccion == 'receta') active @endif ">
				<a class="nav-link" href="{{ route('recetaslista') }}">
					<i class="material-icons">shopping_basket</i>
					<p> Recetas</p>
				</a>
			</li>
			<li class="nav-item @if($seccion == 'localizar') active @endif">
				<a class="nav-link" href="{{ route('localizar') }}">
					<i class="material-icons">view_list</i>
					<p> Localizar </p>
				</a>
			</li>
			<!--li class="nav-item @if($seccion == 'ingredietes') active @endif">
				<a class="nav-link" href="{{ route('consultarticulos') }}">
					<i class="material-icons">image_search</i>
					<p> Consultar ingredientes </p>
				</a>
			</li-->
		</ul>
	</div>
</li>
@endsection