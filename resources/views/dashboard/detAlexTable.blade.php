<table class="table table-condensed table-striped">
	<thead>
		<tr>
			<th>Sucursal</th>							
			<th>Total ($)</th>					
		</tr>
	</head>
	<tbody>
        @php
            $total= 0;
        @endphp
		@foreach($det AS $dato)
            <tr onclick="goto('{{ $dato->idSucursal }}')" ><td class="text-left">{{ $dato->idSucursal }}</td><td  class="text-right">{{ number_format($dato->total,0,"",",") }}</td></tr>
            @php
                $total +=$dato->total ;
            @endphp 
        @endforeach
        <tr><td class="text-left">Total</td><td  class="text-right">{{ number_format($total,0,"",",") }}</td></tr>
	</tbody>
</table>