@extends('layouts.app')
@include('menu.dashalex', ['seccion' => 'dash'])
@section('content')
<div class="row" style="z-index: 1050!important;">
    <div class="col-md-12">
        <div class="card" style="z-index: 1050!important;">
            <div class="card-header">
                <h4 class="card-title">Consultar</h4>
            </div>
            <div class="card-content" style="padding: 10px;">
                <form name="frmAccion" id="frmAccion" action="#">
                    <input type="hidden" name='_token' value="{{ csrf_token() }}">
                    <select name="sucursal" onchange="cambia(this.value)" class="selectpicker">
                        <option value="0" @if(empty($selSuc)) selected @endif> Todas las sucursales</option>
                        @foreach($sucursales as $sucursal)
                            <option @if(!empty($selSuc) && $selSuc==$sucursal->id) selected @endif value="{{ $sucursal->id }}">{{ $sucursal->nombre}}</option>
                        @endforeach
                    </select>
                    @if(!empty($Role) && ( $Role ==1 || ($Role ==6 && !empty($selSuc) ) || ($Role ==2 && !empty($selSuc) )))
                    <div class="row">
                        <div class="col-md-2">
                            <select name="tipo" class="selectpicker">
                                @foreach($tipos as $tipo)
                                @if( $Role==1 || $Role==6 || ($Role==2 && $tipo->id==8) )
                                    <option value="{{$tipo->id}}">{{$tipo->tipo}}</option>
                                @elseif($Role==1 || $Role==6 )
                                    <option value="{{$tipo->id}}">{{$tipo->tipo}}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select name="prioridad" class="selectpicker">
                                <option value="3">Normal</option>
                                <option value="2">Media</option>
                                <option value="1">Alta</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="accion" class="form-control">
                        </div>
                        <div class="col-md-2">
                            <button style="width: 100%" id="saveBtn" class="btn btn-info">Agregar</button>
                        </div>                    
                    </div>
                    @endif
                </form>
            </div>
        </div>
    </div>
</div>

@if(!empty($Role) && ( $Role ==1 || $Role ==6 || $Role ==2 ||!empty($selSuc)))

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Pendientes</h4>
            </div>
            <div class="card-content" style="padding: 10px;">
                <form name="frmUpdate" id="frmUpdate" action="#">
                <input type="hidden" name='_token' value="{{ csrf_token() }}">
                <div class="table-responsive">
                        <table class="table table-striped">
                        <thead>
                            <tr><th style="width: 10% !important;">Tipo</th><th style="width: 10% !important;">Fecha</th><th style="width: 68% !important;">Descripción</th><th style="width: 7% !important;">Acciones</th><th style="width: 5% !important;">Terminar</th></tr>
                        </thead>
                        <tbody id="pendientesBody">
                        @if(!empty($pendientes))
                        @foreach($pendientes as $pendiente)
                            <tr><td>{{$pendiente->tipo}}</td><td>{{ $pendiente->fecha}}</td><td id="td_{{ $pendiente->id}}"><span id="lbl_{{ $pendiente->id}}">
                                @if($pendiente->prioridad < 3)
                                    <i class="material-icons 
                                        @if($pendiente->prioridad < 2) 
                                        text-danger
                                        @else 
                                        text-warning
                                        @endif">flag</i>
                                    @endif {{ $pendiente->accion}}</span></td><td class="td-actions text-right">
                                    @if(!empty($Role) && ( $Role ==1 || ($Role ==6 && !empty($selSuc) ))) <a href="#" onclick="editaPendiente({{ $pendiente->id  }}, {{ empty($pendiente->prioridad) ? 3 :$pendiente->prioridad }})" class="btn btn-success btn-simple"><i class="material-icons">edit</i></a> @endif
                                    <a href="#" onclick="muestraMensajes({{ $pendiente->id  }})" class="btn btn-info btn-simple"><i class="material-icons">open_in_new</i></a>
                            </td><td style="text-align: center;"><input type="hidden" name="ida[]" value="{{$pendiente->id}}"><input type="checkbox" name="accion[{{$pendiente->id}}]"></td></tr>
                        @endforeach                            
                        @endif
                        </tbody>
                    </table>
                </div>@if(!empty($Role) && ( $Role ==1 || ($Role ==6 && !empty($selSuc) )))
                <button  id="updBtn" class="btn btn-info">Actualizar</button>
                @endif
                </form>
            </div>
        </div>  
    </div>        
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Terminadas</h4>
            </div>
            <div class="card-content" style="padding: 10px;">
                <div class="table-responsive">
                        <table class="table table-striped">
                        <thead>
                                <tr><th style="width: 10% !important;">Tipo</th><th style="width: 10% !important;">Fecha</th><th style="width: 70% !important;">Texto</th><th style="width: 7% !important;">Acciones</th></tr>
                        </thead>
                        <tbody id="terminadasBody">
                        @if(!empty($terminadas))
                        @foreach($terminadas as $terminada)
                            <tr><td>{{$terminada->tipo}}</td><td>{{ $terminada->fecha}}</td><td>{{ $terminada->accion}}</td>
                            <td class="td-actions text-right"><a href="#" onclick="muestraMensajes({{ $terminada->id  }})" class="btn btn-info btn-simple"><i class="material-icons">open_in_new</i></a></td>
                            </tr>
                            
                        @endforeach           
                        @endif                 
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>        
</div>
@endif
<!-- Modal -->
<div class="modal fade" id="detModal" tabindex="-1" role="dialog" aria-labelledby="detModalLabel" style="z-index: 1059!important;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="detModalLabel"></h4>
        </div>
        <div class="modal-body">
            <div id="detModalTabContent" class="row">
                <div class="col-md-2">
                    Mensaje:
                </div>
                <div class="col-md-9">
                    <input type="text" id="mensaje" class="form-control">
                </div>
                <div class="col-md-4">
                    <input type="hidden" value="0" id="accion">
                    <button id="saveMensaje" class="btn btn-info btn-sm">Agregar</button>
                </div>  
            </div>
            <div class="row">
                    <div id="listaMensajes" class="col-md-12 ml-auto mr-auto"  style="height: 300px !important; overflow-y: scroll;">

                    </div>
                </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
<!-- Modal -->
@endsection
@section('aditionalScripts')
<style>
        .loader {
          border: 16px solid #f3f3f3;
          border-radius: 50%;
          border-top: 16px solid #3498db;
          width: 120px;
          height: 120px;
          -webkit-animation: spin 2s linear infinite; /* Safari */
          animation: spin 2s linear infinite;
        }
        
        /* Safari */
        @-webkit-keyframes spin {
          0% { -webkit-transform: rotate(0deg); }
          100% { -webkit-transform: rotate(360deg); }
        }
        
        @keyframes spin {
          0% { transform: rotate(0deg); }
          100% { transform: rotate(360deg); }
        }

        .table-sm{
          font-size: 12px;
        }
            
        .table-sm th{
          width: 120px;
          padding: 4px;
        }
        .table-sm td{
          padding: 4px;
        }
        .tdnumber{
            text-align: right !important;
        }
        </style>
<script>
function cambia(page)
{
    $( ".loader" ).remove();
	$("#detModalLabel").text("Cargando espere un momento...");
	$('#detModal').modal('show');
    $("#detModalTabContent").append("<div class='loader'></div>");
    document.location.href = "http://intranet.prigo.com.mx/finanzas/bitacora/"+page;
}

function muestraMensajes(idTarea)
{
    $("#accion").val(idTarea);
    $( ".loader" ).remove();
	$("#detModalLabel").text("Mensajes");
    $('#detModal').modal('show');
    $("#listaMensajes").empty();
    $("#listaMensajes").append("<div class='loader'></div>");

	var params = { "accion": idTarea, "_token": "{{ csrf_token() }}" };
	$.ajax({ 
		type: "POST",
		url: "{{ route('getMensajes') }}",
		data: params,
		success: function(msg){
			$("#listaMensajes").empty();
			$("#listaMensajes").append(msg);
			$( ".loader" ).remove();
		},
		error: function(){
			console.log("error");
		}
	});


    //document.location.href = "http://intranet.prigo.com.mx/finanzas/bitacora/mensajes/"+idTarea;
}

function editaPendiente(idTarea, prioridad){
    $("#lbl_"+idTarea).hide();
    $("#td_"+idTarea).append("<div id='div_"+idTarea+"' style='width: 100%;'><input id='txtTask_"+idTarea+"' style='width: 50%;' type='text'> <select id='selTask_"+idTarea+"'><option value=1 "+(prioridad==1?" SELECTED":"")+">Alta</option><option value=2 "+(prioridad==2?" SELECTED":"")+">Media</option><option value=3 "+(prioridad==3?" SELECTED":"")+">Normal</option></select> <button id='btnTask_"+idTarea+"' class='btn btn-warning btn-sm'>Guardar</button> <button id='btnCancelTask_"+idTarea+"' class='btn btn-danger btn-sm'>Cancelar</button> <button id='btnDeleteTask_"+idTarea+"' class='btn btn-danger btn-sm'><i class='material-icons'>delete_forever</i> Eliminar</button></div>");
    $("#txtTask_"+idTarea).val($("#lbl_"+idTarea).text().replace(/flag/g,'').trim());
    $("#btnDeleteTask_"+idTarea).click(function(e){
        e.preventDefault();
        eliminaTarea(idTarea);
    });
    $("#btnTask_"+idTarea).click(function(e){
        e.preventDefault();
        guardaEdicion(idTarea);
    });
    $("#btnCancelTask_"+idTarea).click(function(e){
        e.preventDefault();
        $("#lbl_"+idTarea).show();
        $("#div_"+idTarea).empty();
        $("#div_"+idTarea).remove();
    });
}

$("#saveMensaje").click(function(e){
    e.preventDefault();
    var idTarea = $("#accion").val();
    $.ajax({
        type: "POST",
        url: "{{ route('guardaMensaje') }}",
        data: { "accion": idTarea, "valor": $("#mensaje").val(), "_token": "{{ csrf_token() }}" },
        success: function(msg){ 
            $("#mensaje").val("");
            $("#listaMensajes").empty();
            $("#listaMensajes").append("<div class='loader'></div>");
            var params = { "accion": idTarea, "_token": "{{ csrf_token() }}" };
            $.ajax({ 
                type: "POST",
                url: "{{ route('getMensajes') }}",
                data: params,
                success: function(msg){
                    $("#listaMensajes").empty();
                    $("#listaMensajes").append(msg);
                    $( ".loader" ).remove();
                },
                error: function(){
                    console.log("error");
                }
            });
        },
        error: function(){
            swal({
                type: 'error',
                title: 'Oops...',
                text: 'Algo ha salido mal!',
                footer: 'Problemas? sit@prigo.com.mx	',
            });
        }
    });
    
});

function eliminaTarea(idTarea) {

    swal({
        title: "Estas segur@?",
        text: "Se eliminara por completo la tarea seleccionada!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        allowOutsideClick: false,
        confirmButtonText: 'Si, actualizar!',
        cancelButtonText: 'No, cancelar!'
    }).then((result) => {
        $.ajax({
            type: "POST",
            url: "{{ route('eliminaTaskBitacora') }}",
            data: { "accion": idTarea, "_token": "{{ csrf_token() }}" },
            success: function(msg){
                $("#lbl_"+idTarea).text($("#txtTask_"+idTarea).val());
                $("#lbl_"+idTarea).show();
                $("#div_"+idTarea).empty();
                $("#div_"+idTarea).remove();

                $("#pendientesBody").empty();
                $("#pendientesBody").load("{{ route('getAcciones')}}/{{$selSuc}}/1");
            },
            error: function(){
                swal({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Algo ha salido mal!',
                    footer: 'Problemas? sit@prigo.com.mx	',
                });
            }
        }); 
    });
}

function guardaEdicion(idTarea) {
    $.ajax({
        type: "POST",
        url: "{{ route('actualizaTaskBitacora') }}",
        data: { "accion": idTarea, "valor": $("#txtTask_"+idTarea).val(), "prioridad": $("#selTask_"+idTarea).val(), "_token": "{{ csrf_token() }}" },
        success: function(msg){ 
            $("#lbl_"+idTarea).text($("#txtTask_"+idTarea).val());
            $("#lbl_"+idTarea).show();
            $("#div_"+idTarea).empty();
            $("#div_"+idTarea).remove();
            location.reload();
        },
        error: function(){
            swal({
                type: 'error',
                title: 'Oops...',
                text: 'Algo ha salido mal!',
                footer: 'Problemas? sit@prigo.com.mx	',
            });
        }
    });
}
$('#updBtn').click(function(e){e.preventDefault();
    $.ajax({
        type: "POST",
        url: "{{ route('actualizaBitacora') }}",
        data: $('#frmUpdate').serialize(),
        success: function(msg){
            obj = JSON.parse(msg);
            if(obj.success)
            {
                $("#terminadasBody").empty();
                $("#terminadasBody").load("{{ route('getAcciones')}}/{{$selSuc}}/2");
                $("#pendientesBody").empty();
                $("#pendientesBody").load("{{ route('getAcciones')}}/{{$selSuc}}/1");
                swal({
                    type: 'success',
                    title: 'Tu solicitud a quedado registrada!'
                });
                
                }
            else{
                swal({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Algo ha salido mal!',
                    footer: 'Problemas? sit@prigo.com.mx	',
                });         }
        },
        error: function(){
            swal({
                type: 'error',
                title: 'Oops...',
                text: 'Algo ha salido mal!',
                footer: 'Problemas? sit@prigo.com.mx	',
            });
        }
    });

});
$('#saveBtn').click(function(e){e.preventDefault();
    $.ajax({
        type: "POST",
        url: "{{ route('guardaBitacora') }}",
        data: $('#frmAccion').serialize(),
        success: function(msg){
            obj = JSON.parse(msg);
            if(obj.success)
            {
                $("#pendientesBody").empty();
                $("#pendientesBody").load("{{ route('getAcciones')}}/{{$selSuc}}/1");
                swal({
                    type: 'success',
                    title: 'Tu solicitud a quedado registrada!'
                });
            }
            else
            {            
                swal({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Algo ha salido mal!',
                    footer: 'Problemas? sit@prigo.com.mx	',
                });         
            }
        },
        error: function(){
            swal({
                type: 'error',
                title: 'Oops...',
                text: 'Algo ha salido mal!',
                footer: 'Problemas? sit@prigo.com.mx	',
            });
        }
    });

});

</script>
@endsection