@extends('layouts.app')
@section('appmenu')
<li class="nav-item active">
	<a class="nav-link" href="#" aria-expanded="true">
		<i class="material-icons">assessment</i>
		<p> Dashboard <b class="caret"></b> </p>
    </a>
	<div class="collapse show" id="pagesExamples" style="">
		<ul class="nav">
			<li class="nav-item">
				<a class="nav-link" href="#">
					<i class="material-icons">thumb_up_alt</i>
					<p> Redes Sociales </p>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="">
					<i class="material-icons">assignment</i>
					<p> Auditoria I. </p>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="">
					<i class="material-icons">local_taxi</i>
					<p> Uber </p>
				</a>
			</li>
		</ul>
	</div>
</li>
@endsection
@section('content')
<div class="row" style="margin-bottom: 10px;">
<div class="col-md-7 ml-auto">
	<ul class="nav nav-pills nav-pills-warning justify-content-right" role="tablist">
		<li class="nav-item">
		<a class="nav-link active show" href="{{ route('panel') }}" role="tablist">
		  Dashboard
		</a>
		</li>
		<li class="nav-item">
		<a class="nav-link" href="{{ route('dashfin') }}" role="tablist">
		  Finanzas
		</a>
		</li>
		<li class="nav-item">
		<a class="nav-link" href="{{ route('dashop') }}" role="tablist">
		  Operaciones
		</a>
		</li>
		<li class="nav-item">
		<a class="nav-link" href="{{ route('dashcom') }}" role="tablist">
		  Compras
		</a>
		</li>
		<li class="nav-item">
		<a class="nav-link" href="{{ route('dashrh') }}" role="tablist">
		  RH
		</a>
		</li>
	</ul>
</div>
</div>
<div class="row">
	<div id="kpiYTDFinanzas-panel-1" class="col-md-4">
		<div class="card ">
			<div class="card-header @if( $ingresoAc - $budgetIngAc < 0 ) card-header-danger @else card-header-success @endif card-header-icon">
				<div class="card-icon">
					<i class="material-icons">attach_money</i>
				</div>
				<h4 class="card-title">Ingreso acumulado</h4>
			</div>
			<div class="card-body ">
				<div class="row">
					<div class="col-md-12 ml-auto mr-auto">
						<h3>{{ number_format($ingresoAc, 0,".",",") }}</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 ml-auto mr-auto">
						Budget: {{ number_format($budgetIngAc, 0,".",",") }}<br>
						Diferencia: <span class="@if($ingresoAc - $budgetIngAc >= 0) success @else danger @endif">{{number_format($ventasaYTDL - $ventasbYTDL, 0,".",",")}}</span>
						
					</div>
				</div>
			</div>
		</div>
	</div>	 
	<div id="kpiYTDFinanzas-panel-1" class="col-md-4">
		<div class="card ">
			<div class="card-header @if( $ingresoAn - $budgetIngAn < 0 ) card-header-danger @else card-header-success @endif card-header-icon">
				<div class="card-icon">
					<i class="material-icons">attach_money</i>
				</div>
				<h4 class="card-title">Ingreso mes anterior</h4>
			</div>
			<div class="card-body ">
				<div class="row">
					<div class="col-md-12 ml-auto mr-auto">
						<h3>{{ number_format($ingresoAn, 0,".",",") }}</H3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 ml-auto mr-auto">
						Budget: {{ number_format($budgetIngAn, 0,".",",") }}<br>
						Diferencia: <span class="@if($ingresoAn - $budgetIngAn >= 0) success @else danger @endif">{{number_format($ingresoAn - $budgetIngAn, 0,".",",")}}</span><br>
					</div>
				</div>
			</div>
		</div>
	</div>	 
	<div class="col-lg-4 col-md-4 col-sm-6">
		<div class="card ">
			<div class="card-header @if( $ingresoMes - $budgetIngMes < 0 ) card-header-danger @else card-header-success @endif card-header-icon">
				<div class="card-icon">
					<i class="material-icons">attach_money</i>
				</div>
				<h4 class="card-title">Ingreso {{ $mes }}</h4>
			</div>
			<div class="card-body ">
				<div class="row">
					<div class="col-md-12 ml-auto mr-auto">
						<h3>$ {{ number_format($ingresoMes, 0,".",",") }}</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 ml-auto mr-auto">
						Budget: {{ number_format($budgetIngMes, 0,".",",") }}<br>
						Diferencia: {{number_format($ingresoMes - $budgetIngMes, 0,".",",")}}
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-6">
		<div class="card">
		  <div class="card-header @if( $costoAc - $budgetCosAc  < 0 ) card-header-success @else card-header-danger @endif card-header-icon">
			<div class="card-icon">
			  <i class="material-icons">@if(  $costoAc - $budgetCosAc > 0 ) trending_up @else trending_down @endif</i>
			</div>
			<h4 class="card-title">FoodCost Acumulado</h4>
		  </div>
		  <div class="card-body">
			<h3>{{ number_format(($costoAc/$ingresoAc*100),2) }} %</h3>
		  </div>
		  <div class="card-footer">
			<div class="stats">
			  Costo: {{ number_format($costoAc,2) }}<br>
			  Budget: {{ number_format($budgetCosAc,2) }}<br>
			  Diferencia: {{ number_format($costoAc - $budgetCosAc,2) }}
			</div>
		  </div>
		</div>
	</div>	
	<div class="col-lg-4 col-md-4 col-sm-6">
		<div class="card">
		  <div class="card-header @if( $costoAn - $budgetCosAn < 0 ) card-header-success @else card-header-danger @endif card-header-icon">
			<div class="card-icon">
			  <i class="material-icons">@if( $costoAn - $budgetCosAn > 0 ) trending_up @else trending_down @endif</i>
			</div>
			<h4 class="card-title">FoodCost mes anterior</h4>
		  </div>
		  <div class="card-body">
			<h3>{{ number_format(($costoAn/$ingresoAn*100),2) }} %</h3>
		  </div>
		  <div class="card-footer">
			<div class="stats">
			  Costo: {{ number_format($costoAn,2) }} <br>
			  Budget: {{ number_format($budgetCosAn,2) }} <br>
			  Diferencia: {{number_format($costoAn - $budgetCosAn,2) }}
			</div>
		  </div>
		</div>
	</div>	
	<div class="col-lg-4 col-md-4 col-sm-6">
		<div class="card">
		  <div class="card-header @if( $budgetCosMes - $costoMes < 0 ) card-header-danger @else card-header-success @endif card-header-icon">
			<div class="card-icon">
			  <i class="material-icons">@if( $budgetCosMes - $costoMes < 0 ) trending_up @else trending_down @endif</i>
			</div>
			<h4 class="card-title">FoodCost {{ $mes }}</h4>
		  </div>
		  <div class="card-body">
			<h3>{{ number_format(($costoMes/(empty($ingresoMes)?1:$ingresoMes)*100),2) }} % </h3>
		  </div>
		  <div class="card-footer">
			<div class="stats">
			  Costo: {{ number_format($costoMes,2) }} <br>
			  Budget: {{ number_format($budgetCosMes,2) }} <br>
			  Diferencia: {{ number_format($budgetCosMes - $costoMes,2) }}
			</div>
		  </div>
		</div>
	</div>
	
	<div class="col-lg-4 col-md-4 col-sm-6">
		<div class="card">
		  <div class="card-header @if( $difebitda < 0 ) card-header-danger @else card-header-success @endif card-header-icon">
			<div class="card-icon">
			  <i class="material-icons">@if( $difebitda > 0 ) trending_up @else trending_down @endif</i>
			</div>
			<h4 class="card-title">EBIT Acumulado</h4>
		  </div>
		  <div class="card-body">
			<h3>{{ number_format($ebitAc,2) }} %</h3>
		  </div>
		  <div class="card-footer">
			<div class="stats">
			  Budget: {{ number_format($budgetEbitAc,2) }}% <br>
			  Diferencia: {{ number_format($ebitAc - $budgetEbitAc,2) }}%			  
			</div>
		  </div>
		</div>
	</div>	
	<div class="col-lg-4 col-md-4 col-sm-6">
		<div class="card">
		  <div class="card-header @if( $difebitda < 0 ) card-header-danger @else card-header-success @endif card-header-icon">
			<div class="card-icon">
			  <i class="material-icons">@if( $difebitda > 0 ) trending_up @else trending_down @endif</i>
			</div>
			<h4 class="card-title">EBIT mes anterior</h4>
		  </div>
		  <div class="card-body">
			<h3>{{ number_format($ebitAn,2) }} %</h3>
		  </div>
		  <div class="card-footer">
			<div class="stats">
				Budget: {{ number_format($budgetEbitAn,2) }}%<br>
				Diferencia: {{ number_format($ebitAn - $budgetEbitAn,2) }}%
			</div>
		  </div>
		</div>
	</div>	
	<div class="col-lg-4 col-md-4 col-sm-6">
		<div class="card">
		  <div class="card-header @if( $difebitda < 0 ) card-header-danger @else card-header-success @endif card-header-icon">
			<div class="card-icon">
			  <i class="material-icons">@if( $difebitda > 0 ) trending_up @else trending_down @endif</i>
			</div>
			<h4 class="card-title">EBIT {{ $mes }}</h4>
		  </div>
		  <div class="card-body">
			<h3>{{ number_format($ebitMes,2) }} %</h3>
		  </div>
		  <div class="card-footer">
			<div class="stats">
			  Diferencia: {{ number_format($budgetEbitMes,2) }}% <br>
			  Budget: {{ number_format($ebitMes-$budgetEbitMes,2) }}%
			</div>
		  </div>
		</div>
	</div>
	<div class="col-lg-3 col-md-6 col-sm-6">
		<div class="card">
		  <div class="card-header @if( $crecimientoAnual <= 0 ) card-header-danger @else card-header-success @endif card-header-icon">
			<div class="card-icon">
			  <i class="material-icons">@if( $crecimientoAnual > 0 ) trending_up @else trending_down @endif</i>
			</div>
			<h4 class="card-title">Crecimiento Anual</h4>
		  </div>
		  <div class="card-body"><h3 onclick="getDetGrowth(1)" >{{ number_format($crecimientoAnual,2) }} %</h3></div>
		  <div class="card-footer">
			<div class="stats">
			  CRECIMIENTO EN INGRESO RESPECTO AL AÑO PASADO (MISMAS TIENDAS) 
			</div>
		  </div>
		</div>
	</div>
	<div class="col-lg-3 col-md-6 col-sm-6">
		<div class="card">
		  <div class="card-header @if( $crecimientoAnualizado <= 0 ) card-header-danger @else card-header-success @endif card-header-icon">
			<div class="card-icon">
			  <i class="material-icons">@if( $crecimientoAnualizado > 0 ) trending_up @else trending_down @endif</i>
			</div>
			<h4 class="card-title">Crecimiento Anualizado</h4>
		  </div>
		  <div class="card-body"><h3>{{ number_format($crecimientoAnualizado,2) }} %</h3></div>
		  <div class="card-footer">
			<div class="stats">
			  CRECIMIENTO EN INGRESO RESPECTO AL AÑO PASADO 
			</div>
		  </div>
		</div>
	</div>
	<div class="col-lg-3 col-md-6 col-sm-6">
		<div class="card">
		  <div class="card-header @if( $crecimientoMes <= 0 ) card-header-danger @else card-header-success @endif card-header-icon">
			<div class="card-icon">
			  <i class="material-icons">@if( $crecimientoMes > 0 ) trending_up @else trending_down @endif</i>
			</div>
			<h4 class="card-title">Crecimiento {{ $mes }}</h4>
		  </div>
		  <div class="card-body">
			<h3 onclick="getDetGrowth(2)" >{{ number_format($crecimientoMes,2) }} %</h3>
		  </div>
		  <div class="card-footer">
			<div class="stats">
			  CRECIMIENTO EN VENTAS RESPECTO AL AÑO PASADO {{ $mes }}
			</div>
		  </div>
		</div>
	</div>
	
	@foreach( $cheque as $dato)
	
	<div class="col-lg-3 col-md-6 col-sm-6">
		<div class="card">
		  <div class="card-header card-header-info card-header-icon">
			<div class="card-icon">
			  <i class="material-icons">store</i>
			</div>
			<h4 class="card-title">{{ $dato->rvc }}</h4>
		  </div>
		  <div class="card-body">
				<b>Cheque Promedio Anual:</b> <br>{{ number_format($dato->cheque,2) }} <br>
				<b>Clientes Promedio Anual:</b> <br>{{ number_format($dato->clientes,2) }}<br>
				<b>Venta Promedio Anual:</b> <br>	{{ number_format($dato->venta,2) }}
		  </div>
		  <div class="card-footer">

		  </div>
		</div>
	</div>
	
	@endforeach
</div>

<div class="modal fade" id="detGrowthModal" tabindex="-1" role="dialog" aria-labelledby="detModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="detGrowthModallLabel"></h4>
      </div>
      <div class="modal-body">
		<div class="row">
			<div id="detModalTabContent" class="col-md-12 ml-auto mr-auto"  style="height: 300px !important; overflow-y: scroll;">
				
			</div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('aditionalScripts')
<script>
$().ready(function(){
	/*
	var datakpiYTDFinanzas = {
		labels: ['Ventas', 'Budget'],
		series: [
			[{{ $ventasaYTD }}],
			[{{ $ventasbYTD }}]
		]
	};

	var optionskpiYTDFinanzas = {
		axisY: {
			offset: 80
		},
		legend: {
			display: true			
		},
		chartPadding: {
			top: 0,
			right: 0,
			bottom: 0,
			left: 10
		}
	};
	var kpiYTDFinanzasChart = new Chartist.Bar('#kpiVentasYTD', datakpiYTDFinanzas, optionskpiYTDFinanzas);
	// start animation for the Completed Tasks Chart - Line Chart
	md.startAnimationForLineChart(kpiYTDFinanzasChart);
	
	var chartCosto = new Chartist.Bar('#kpicosto',{labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago'],series: [ [{{	$kpiGastoMes }} ]]})
	md.startAnimationForLineChart(chartCosto);
	
	var chartGasto = new Chartist.Line('#kpigasto', {
	  labels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago'],
	  series: [ [{{	$kpiGastoMes }} ]]
	}, {
		axisY: {
			offset: 100
		},
		axisX: {
			labelInterpolationFnc: function(value) {
				return value.split(/\s+/).map(function(word) {
					return word[0];
				}).join('');
			}
		},
		lineSmooth: Chartist.Interpolation.cardinal({
			tension: 0
		}),
		low: {{$minGastoMes - 1000 }},
		high: {{$maxGastoMes + 1000}},
		showArea: false,
		showPoint: false,
		fullWidth: true
	});

	md.startAnimationForLineChart(chartGasto);
	var kpiMTDFinanzasChart = new Chartist.Pie('#kpiVentasMTD', {
  series: [50,20,30]
}, {
  donut: true,
  donutWidth: 40,
  startAngle: 270,
  total: 200,
  showLabel: false
});
	
	md.startAnimationForLineChart(kpiMTDFinanzasChart);
	
	

var data = {
  series: [1050,236]
};

var sum = function(a, b) { return a + b };

new Chartist.Pie('#kpiFuerzaLab', data, {
  labelInterpolationFnc: function(value) {
	console.log(value);
	console.log(Math.round(value / 1286 * 100));
    return Math.round(value / 1286 * 100) + '%';
  }
});
*/	
	
});

function getDetGrowth(tipo)
{
	var nom = "Detalle Crecimiento Anual";
	if(tipo==2)
		nom = "Detalle Crecimiento {{ $mes }}";
	
	$( ".loader" ).remove();
	$("#detGrowthModallLabel").text(nom);
	$('#detGrowthModal').modal('show');
	$("#detGrowthModalTabContent").append("<div class='loader'></div>");
	
	
}
</script>
@endsection